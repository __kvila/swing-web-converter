package main.java.com.swingconverter;

import main.java.com.swingconverter.service.ConverterGUIService;

import javax.swing.*;

public class ConverterMain {
    public static void main(String[] args) {
        ConverterGUIService converter = new ConverterGUIService();
        converter.setVisible(true);
    }
}
