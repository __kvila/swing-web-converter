function convertTime(){
  var second = document.getElementById("sec").value;
  if(document.getElementById("selectTime").value === "Min"){
    document.getElementById("secResult").value = second / 60; 
  } else if(document.getElementById("selectTime").value === "Hour"){
    document.getElementById("secResult").value = second / 3600;
  } else if(document.getElementById("selectTime").value === "Day"){
    document.getElementById("secResult").value = second / 86400;
  } else if(document.getElementById("selectTime").value === "Week"){
    document.getElementById("secResult").value = second / 604800;
  } else if(document.getElementById("selectTime").value === "Month"){
    document.getElementById("secResult").value = parseInt(second) / 2678400;
  } else if(document.getElementById("selectTime").value === "Astronomical Year"){
    document.getElementById("secResult").value = parseInt(second) / 32140800;
  }
}

function backTime(){
  var time = document.getElementById("secResult").value;
  if(document.getElementById("selectTime").value === "Min"){
    document.getElementById("sec").value = time * 60; 
  } else if(document.getElementById("selectTime").value === "Hour"){
    document.getElementById("sec").value = time * 3600;
  } else if(document.getElementById("selectTime").value === "Day"){
    document.getElementById("sec").value = time * 86400;
  } else if(document.getElementById("selectTime").value === "Week"){
    document.getElementById("sec").value = time * 604800;
  } else if(document.getElementById("selectTime").value === "Month"){
    document.getElementById("sec").value = time * 2678400;
  } else if(document.getElementById("selectTime").value === "Astronomical Year"){
    document.getElementById("sec").value = time * 32140800;
  }
}

function convertWeight(){
  var kg = document.getElementById("kg").value;
  if(document.getElementById("selectWeight").value === "g"){
    document.getElementById("kgResult").value = kg * 1000; 
  } else if(document.getElementById("selectWeight").value === "carat"){
    document.getElementById("kgResult").value = kg * 5000;
  } else if(document.getElementById("selectWeight").value === "eng pound"){
    document.getElementById("kgResult").value = kg * 0.45359237;
  } else if(document.getElementById("selectWeight").value === "pound"){
    document.getElementById("kgResult").value = kg * 2.20462262185;
  } else if(document.getElementById("selectWeight").value === "stone"){
    document.getElementById("kgResult").value = kg * 0.157473;
  } else if(document.getElementById("selectWeight").value === "rus pound"){
    document.getElementById("kgResult").value = kg * 2.441899995502;
  }
}

function backWeight(){
  var weight = document.getElementById("kgResult").value;
  if(document.getElementById("selectWeight").value === "g"){
    document.getElementById("kg").value = weight / 1000; 
  } else if(document.getElementById("selectWeight").value === "carat"){
    document.getElementById("kg").value = weight / 5000;
  } else if(document.getElementById("selectWeight").value === "eng pound"){
    document.getElementById("kg").value = weight / 0.45359237;
  } else if(document.getElementById("selectWeight").value === "pound"){
    document.getElementById("kg").value = weight / 2.20462262185;
  } else if(document.getElementById("selectWeight").value === "stone"){
    document.getElementById("kg").value = weight / 0.157473;
  } else if(document.getElementById("selectWeight").value === "rus pound"){
    document.getElementById("kg").value = weight / 2.441899995502;
  }
}

function convertVolume(){
  var l = document.getElementById("l").value;
  if(document.getElementById("selectVolume").value === "m^3"){
    document.getElementById("lResult").value = l * 0.001; 
  } else if(document.getElementById("selectVolume").value === "gallon"){
    document.getElementById("lResult").value = l * 0.26417205;
  } else if(document.getElementById("selectVolume").value === "pint"){
    document.getElementById("lResult").value = l * 1.7597504;
  } else if(document.getElementById("selectVolume").value === "quart"){
    document.getElementById("lResult").value = l * 1.05669;
  } else if(document.getElementById("selectVolume").value === "barrel"){
    document.getElementById("lResult").value = l * 0.00628981;
  } else if(document.getElementById("selectVolume").value === "cubic foot"){
    document.getElementById("lResult").value = l * 0.03531467;
  } else if(document.getElementById("selectVolume").value === "cubic inch"){
    document.getElementById("lResult").value = l * 61.023744;
  }
}

function backVolume(){
  var volume = document.getElementById("lResult").value;
  if(document.getElementById("selectVolume").value === "m^3"){
    document.getElementById("l").value = volume / 0.001; 
  } else if(document.getElementById("selectVolume").value === "gallon"){
    document.getElementById("l").value = volume / 0.26417205;
  } else if(document.getElementById("selectVolume").value === "pint"){
    document.getElementById("l").value = volume / 1.7597504;
  } else if(document.getElementById("selectVolume").value === "quart"){
    document.getElementById("l").value = volume / 1.05669;
  } else if(document.getElementById("selectVolume").value === "barrel"){
    document.getElementById("l").value = volume / 0.00628981;
  } else if(document.getElementById("selectVolume").value === "cubic foot"){
    document.getElementById("l").value = volume / 0.03531467;
  } else if(document.getElementById("selectVolume").value === "cubic inch"){
    document.getElementById("l").value = volume / 61.023744;
  }
}

function convertLength(){
  var m = document.getElementById("m").value;
  if(document.getElementById("selectLength").value === "km"){
    document.getElementById("mResult").value = m / 1000; 
  } else if(document.getElementById("selectLength").value === "mile"){
    document.getElementById("mResult").value = m * 0.000621371;
  } else if(document.getElementById("selectLength").value === "nautical mile"){
    document.getElementById("mResult").value = m * 0.0005399568;
  } else if(document.getElementById("selectLength").value === "cable"){
    document.getElementById("mResult").value = m * 0.0053961182483768;
  } else if(document.getElementById("selectLength").value === "league"){
    document.getElementById("mResult").value = m * 0.0002071237;
  } else if(document.getElementById("selectLength").value === "foot"){
    document.getElementById("mResult").value = m * 3.280839895;
  } else if(document.getElementById("selectLength").value === "yard"){
    document.getElementById("mResult").value = m * 1.0936132983;
  }
}

function backLength(){
  var length = document.getElementById("mResult").value;
  if(document.getElementById("selectLength").value === "km"){
    document.getElementById("m").value = length * 1000; 
  } else if(document.getElementById("selectLength").value === "mile"){
    document.getElementById("m").value = length / 0.000621371;
  } else if(document.getElementById("selectLength").value === "nautical mile"){
    document.getElementById("m").value = length / 0.0005399568;
  } else if(document.getElementById("selectLength").value === "cable"){
    document.getElementById("m").value = length / 0.0053961182483768;
  } else if(document.getElementById("selectLength").value === "league"){
    document.getElementById("m").value = length / 0.0002071237;
  } else if(document.getElementById("selectLength").value === "foot"){
    document.getElementById("m").value = length / 3.280839895;
  } else if(document.getElementById("selectLength").value === "yard"){
    document.getElementById("m").value = length / 1.0936132983;
  }
}

function convertTemperature(){
  var C = parseInt(document.getElementById("C").value);
  if(document.getElementById("selectTemperature").value === "K"){
    document.getElementById("CResult").value = C + 273.15; 
  } else if(document.getElementById("selectTemperature").value === "F"){
    document.getElementById("CResult").value = C * 9 / 5 + 32;
  } else if(document.getElementById("selectTemperature").value === "Re"){
    document.getElementById("CResult").value = C * 4 / 5;
  } else if(document.getElementById("selectTemperature").value === "Ro"){
    document.getElementById("CResult").value = C * 0.525 + 7.5;
  } else if(document.getElementById("selectTemperature").value === "Ra"){
    document.getElementById("CResult").value = (C + 273.15) * 9 / 5;
  } else if(document.getElementById("selectTemperature").value === "N"){
    document.getElementById("CResult").value = C * 0.33;
  } else if(document.getElementById("selectTemperature").value === "D"){
    document.getElementById("CResult").value = C * 1.5 - 100;
  }
}

function backTemperature(){
  var temperature = parseInt(document.getElementById("CResult").value);
  if(document.getElementById("selectTemperature").value === "K"){
    document.getElementById("C").value = temperature - 273.5; 
  } else if(document.getElementById("selectTemperature").value === "F"){
    document.getElementById("C").value = (temperature - 32) * 5 / 9;
  } else if(document.getElementById("selectTemperature").value === "Re"){
    document.getElementById("C").value = temperature * 5 / 4;
  } else if(document.getElementById("selectTemperature").value === "Ro"){
    document.getElementById("C").value = (temperature - 7.5) / 0.525;
  } else if(document.getElementById("selectTemperature").value === "Ra"){
    document.getElementById("C").value = (temperature - 491.67) * 5 / 9;
  } else if(document.getElementById("selectTemperature").value === "N"){
    document.getElementById("C").value = temperature / 0.33;
  } else if(document.getElementById("selectTemperature").value === "D"){
    document.getElementById("C").value = (temperature + 100) / 1.5;
  }
}